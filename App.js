import React, {Component, useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
  StatusBar,
  Button,
  AsyncStorage,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import SplashScreen from 'react-native-splash-screen';

var results = [];
const STORAGE_KEY = '@save_rule_status';
var scoreObtained = 0;


const wait = (timeout) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}


const Drawer = createDrawerNavigator();

class HomeScreen extends Component{

  constructor(props){
    super(props);
    this.state = {
      tests: []
    };
  }
  async getData(id){
    return await fetch('http://tgryl.pl/quiz/test/' + id)
          .then((response) => response.json())
          .then((json) => {
            return json
          })
          .catch((error) => console.error(error));
  }
  async navigateTest(navigation,item){
    const test = await this.getData(item.id);
    navigation.navigate(item.name , {name: item.name, test: test.tasks, questionIndex: 0, numberOfTasks: item.numberOfTasks})
  }
  componentDidMount(){
    fetch('http://tgryl.pl/quiz/tests')
          .then((response) => response.json())
          .then((json) => {
            this.setState({ tests: json });
          })
          .catch((error) => console.error(error));
  }

  render(){
    const tests = this.state.tests;
    const navigation = this.props.navigation;


    return(
        <View style={styles.container}>
          <View style={styles.main}>
            <View style={styles.header}>
              <Text style={styles.headerText}>Home Page</Text>
              <TouchableOpacity style={styles.imgMenu} onPress={() => {navigation.openDrawer();}}>
                <Image style={styles.img} source={require('./menu.png')}/>
              </TouchableOpacity>
            </View>
            <View style={styles.body}>
              <SafeAreaView style={styles.safAre}>
                <FlatList data={tests}
                          keyExtractor={(item) => item.id}
                          renderItem={({item}) => (
                            <TouchableOpacity style={styles.itemsInList} onPress={() => {this.navigateTest(navigation, item)}}>
                              <Text style={styles.titleItem}>{item.name}</Text>
                              <View style={styles.tags}>
                                {
                                  item.tags.map(mapValue => (
                                    <Text key={mapValue.toString()} style={styles.tagsText}>#{mapValue.toString()}</Text>
                                  ))
                                }
                              </View>
                              <Text>{item.description}</Text>
                            </TouchableOpacity>
                            )}
                          />
              </SafeAreaView>
              <View style={styles.rankingResults}>
                <Text style={styles.txt}>Get to know your ranking result</Text>
                <Button title="Check!" color="grey" onPress={() => navigation.navigate('Result')}/>
              </View>
            </View>
          </View>
        </View>
  );
  };
}

function termsOfUse({navigation}){
  const [accepted, setAccepted] = useState(false);
  useEffect(() => {
    getData();
  }, []);

  if(accepted == true){
    navigation.navigate("Home");
  }

  const onAccept = async() => {
    setAccepted(true);
    await AsyncStorage.setItem(STORAGE_KEY, 'true');
  }

  const getData = async() => {
    const value = await AsyncStorage.getItem(STORAGE_KEY);
    if(value != null){
      if(value){
        setAccepted(true);
      }
      else{
        setAccepted(false);
      }
    }
  }

  return(
    <View style={styles.container}>
      <View style={styles.main}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Terms of use</Text>
          <TouchableOpacity style={styles.imgMenu} onPress={() => {navigation.openDrawer();}}>
            <Image style={styles.img} source={require('./menu.png')}/>
          </TouchableOpacity>
        </View>
        <View style={styles.bodyTable}>
          <Text>Terms of use for application</Text>
          <Text>Terms of use for application</Text>
          <Text>Terms of use for application</Text>
          <Text>Terms of use for application</Text>
          <Text>Terms of use for application</Text>
          <Button title={"Akceptuj"} onPress={()=> {
            onAccept();
            navigation.navigate('Home');
          }}></Button>
        </View>
      </View>
    </View>

  );
}


function resultScreen({navigation}){
  const [refreshing, setRefreshing] = React.useState(false);
  const [data, setData] = useState([]);


  useEffect(() => {
    fetch('http://tgryl.pl/quiz/results')
      .then((response) => response.json())
      .then((json) => setData(json.reverse()))
      .catch((error) => console.error(error))
      ;
  }, []);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(1000).then(() => {
      fetch('http://tgryl.pl/quiz/results')
      .then((response) => response.json())
      .then((json) => setData(json.reverse()))
      .catch((error) => console.error(error))
      setRefreshing(false)
    });
  }, []);
  return(
    <View style={styles.container}>
      <View style={styles.main}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Result</Text>
          <TouchableOpacity style={styles.imgMenu} onPress={() => {navigation.openDrawer();}}>
            <Image style={styles.img} source={require('./menu.png')}/>
          </TouchableOpacity>
        </View>
        <View style={styles.bodyTable}>
            <SafeAreaView>
              <ScrollView refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>
              }>
              <View style={styles.ttable}>
                <Text style={styles.tableHeadStyle}>Nick</Text>
                <Text style={styles.tableHeadStyle}>Points</Text>
                <Text style={styles.tableHeadStyle}>Type</Text>
                <Text style={styles.tableHeadStyle}>Date</Text>
              </View>
              <View>
                <FlatList
                  data={data}
                  keyExtractor={({id}, index) => id}
                    renderItem={({item}) => (
                          <View style={styles.ttable}>
                            <Text style={styles.tableContent}>{item.nick}</Text>
                              <View style={styles.tableContentText}>
                                  <Text>{item.score}</Text>
                                  <Text>/</Text>
                                  <Text>{item.total}</Text>
                              </View>
                            <Text style={styles.tableContent}>{item.type}</Text>
                            <Text style={styles.tableContent}>{item.createdOn}</Text>
                            </View>
                          )
                        }
                />
              </View>
              </ScrollView>
            </SafeAreaView>
        </View>
      </View>
    </View>
    );
} //Ready

function testScreen({route, navigation}){
  const title = route.params.name;
  const test = route.params.test;
  const qIndex = route.params.questionIndex;
  const testLength = route.params.numberOfTasks;

  return(
    <View style={styles.container}>
      <View style={styles.main}>
        <View style={styles.header}>
          <Text style={styles.headerText}>{title}</Text>
          <TouchableOpacity style={styles.imgMenu} onPress={() => {navigation.openDrawer();}}>
            <Image style={styles.img} source={require('./menu.png')}/>
          </TouchableOpacity>
        </View>
        <View style={styles.bodyTestScreen}>
            {testLength > qIndex ? renderQuestion({navigation}, title, test, qIndex, testLength):score({navigation}, title, testLength)}
        </View>

      </View>
    </View>
  );
}



function renderQuestion({navigation}, title, test, qIndex, testLength){
  const [key, setKey] = useState(0);

  return(
    <View style={styles.bodyTestScreen}>
			<View style={styles.infoAboutQuestion}>
			<Text style={styles.questionNumber}>Question {qIndex+1} of {testLength}</Text>
			<Text style={styles.questionTimer}>Time: 28 sec</Text>
			</View>
			<Image style={styles.imgLoadBar} source={require('./load-bar.png')}/>
			<Text style={styles.question}>{test[qIndex].question}</Text>
			<View style={styles.optionsBox}>
          {
               test[qIndex].answers.map(mapValue => (
                 <TouchableOpacity style ={styles.abOption} onPress = {() => {
                                               if (mapValue.isCorrect) {
                                                 scoreObtained++;
                                               }
                                               setKey(prevKey => prevKey + 1);
                                               nextQuestion({navigation},title,test,qIndex,testLength)
                                             }}><Text>{mapValue.content}</Text></TouchableOpacity>
             ))
             }
      </View>
    </View>
    );
  }



function nextQuestion({navigation}, title, test, qIndex, testLength){
  if(qIndex - 1 < testLength){
    navigation.navigate(title, {
      name: title,
      test: test,
      questionIndex: qIndex +1,
      numberOfTasks: testLength
    })
  }
}

function score({navigation}, title, testLength){
  fetch('http://tgryl.pl/quiz/result', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      nick: "Ale łatwo",
      score: scoreObtained,
      total: testLength,
      type: title
    })
  })
  console.log(scoreObtained);
  scoreObtained = 0;
  navigation.navigate("Result");
}


export default class App extends Component{

  constructor(props){
      super(props);
      this.state = {
        tests: []
      };
    }

  componentDidMount(){
    fetch('http://tgryl.pl/quiz/tests')
          .then((response) => response.json())
          .then((json) => {
            this.setState({ tests: json });
          })
          .catch((error) => console.error(error));
    SplashScreen.hide();
  }
  render(){
    const tests = this.state.tests;
    return(
      <NavigationContainer>
        <Drawer.Navigator initialRouteName="Rules">
          <Drawer.Screen name="Home" component={HomeScreen}/>
          <Drawer.Screen name="Result" component={resultScreen}/>
          <Drawer.Screen name="Rules" component={termsOfUse} />
          {
            tests.map(mapValue => (
              <Drawer.Screen name={mapValue.name} component={testScreen}/>
            ))
          }
        </Drawer.Navigator>
      </NavigationContainer>

      );
  }

}
