const React = require('react-native');
const {StyleSheet} = React;

export default {
  container:{
    flex:1,
    padding: '2%',
    justifyContent: 'center',
  },
  header:{
    height: '15%',
    backgroundColor: '#fff',
    textAlign: 'center',
    borderBottomWidth: 2,
    borderBottomColor: '#000',
    flexDirection: 'row-reverse',
    justifyContent: 'center',
    alignItems: 'center',
  },
  main:{
    borderWidth: 5,
    borderColor: 'lightgrey',
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
  },
  headerText:{
    fontSize: 30,
  },
  img:{
    height: 30,
    width: 30,
  },
  imgMenu:{
    paddingRight: '15%',
  },
  body:{
    backgroundColor: '#fff',
    height: '85%',
    width: '100%',
  },
  safAre:{
    backgroundColor: "#fff",
    margin: "5%",
    height: '90%',
    width: '90%',
    flex: 4,
  },
  itemsInList:{
    borderWidth: 3,
    borderColor: 'lightgrey',
    marginTop: 20,
    marginBottom: 20,
    padding: 20,
  },
  tags:{
    flexDirection: 'row',
    marginBottom: 20,
  },
  onetag:{
    color: "blue",
    paddingLeft: 5,
  },
  titleItem:{
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 20
  },
  rankingResults:{
    borderWidth: 2,
    borderColor: 'grey',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txt:{
    marginBottom:10,
    fontSize: 20,
  },
  textTable:{
    margin: 20,
    fontSize: 15,
    justifyContent: 'center',
    textAlign: 'center'
  },
  tableHeadStyle:{
    backgroundColor: 'grey',
    flex:1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: "black",
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 10
  },
  bodyTable:{
    paddingTop: 20,
    paddingLeft: 5,
    paddingRight: 5,
  },
  questionNumber:{
    textAlign: 'left',
    fontSize: 20,
    paddingLeft: 10,
  },
  bodyTestScreen:{
    paddingTop: 30,
    paddingLeft: 15,
    paddingRight: 15,
  },
  infoAboutQuestion:{
    flexDirection: 'row',
  },
  questionTimer:{
    textAlign: 'right',
    fontSize: 20,
    paddingLeft: 70,
  },
  question:{
    fontSize: 20,
    textAlign: 'center',
    marginTop: 10,
  },
  questionInfo:{
    textAlign: 'center',
  },
  optionsBox:{
    marginTop: 30,
    borderWidth: 1,
    height: 270,
    alignItems: 'center',
  },
  optionButton:{
    borderWidth: 1,
    height: 50,
    width: 125,
    backgroundColor: 'lightgrey',
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  abOption:{
    marginTop: 40,
    flexDirection: 'row',

  },
  cdOption:{
    flexDirection: 'row',
    marginTop: 60,
    justifyContent: 'space-around',
  },
  imgLoadBar:{
    height: 40,
    width: 200,
    marginLeft: 40,
    marginTop: 10,
  },
  tableContent:{
    flex:1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: "black",
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 10
  },
  tableContentText:{
    flex:1,
    flexDirection: "row",
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: "black",
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 10
  },
  ttable: {
    flex: 1,
    flexDirection: "row",
    borderWidth: 1,
    borderBottomWidth: 0,
    borderColor: "black"
  },
  tagsText:{
    color: "blue",
    paddingLeft: 5,
    textDecorationLine: "underline"
  }
}
